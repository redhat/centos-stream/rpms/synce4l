#!/bin/bash
# Test if synce4l can parse default configuration

set -ex

# Find a nonexistent network device
i=1
while ip link show eth$i &> /dev/null; do
	i=$((i + 1))
done
eth=eth$i

tmpconf=$(mktemp)

sed \
	-e "s/^\[ens.*\]/\[eth$i\]/" \
	-e "s|/run/synce4l_socket|/doesnotexist/synce4l_socket|" \
	< /etc/synce4l.conf > "$tmpconf"

out=$(timeout -s 9 5 synce4l -m -q -l 7 -f "$tmpconf" 2>&1 || :)

echo "$out" | grep -q "created num_devices:"
echo "$out" | grep -q "synce_manager_server_thread Bind failed"

echo "unknownoption 1" >> "$tmpconf"

out=$(timeout -s 9 5 synce4l -m -q -l 7 -f "$tmpconf" 2>&1 || :)

echo "$out" | grep -q "failed to parse configuration file"

rm "$tmpconf"

exit 0
